ExFilePicker
============

ExFilePicker is an open source android library that allows developers to easily implement choosing files and directories in application.

## Screenshotes:

[![Screenshot](stuff/preview-screenshot1.png)](stuff/screenshot1.png) [![Screenshot](stuff/preview-screenshot2.png)](stuff/screenshot2.png) [![Screenshot](stuff/preview-screenshot3.png)](stuff/screenshot3.png)

## Features

It can:
* Choose one file
* Choose one directory
* Choose few files and/or directories
* Filter by file extension
* Sorting
* Creating directories

## Download

* Gradle:

```groovy
allprojects {
      repositories {
           maven {
               url 'https://jitpack.io'
           }
      }
}
```


```
dependencies {
        implementation 'com.gitlab.joielechong:exfilepicker:3.0.3'
}
```

## Usage

__1.__ Add ExFilePicker library as a dependency to your project

__2.__ Use methods from `ExFilePicker` class to launch picker activity and receive result in `ResultListener`:

```java

	ExFilePicker exFilePicker = ExFilePicker.with(this);
	exFilePicker.resultListener(new ExFilePicker.ExFilePickerListener() {
          @Override public void onResult(ExFilePickerResult result) {
            // Here is result object contains selected files names and path
          }
        });

    exFilePicker.show(); // Show the picker.

```

## Configuration

Class ExFilePicker have following methods for configuration:

* setCanChooseOnlyOneItem() - if true, user can select only one item. False by default.

* setShowOnlyExtensions() - only files with this extensions will showed.

* setExceptExtensions() - files with this extensions will excluded.

* setChoiceType() - one of value from ChoiceType. Set what user can select - only files, only directories or both. Both by default.

* setStartDirectory() - This path will be open when ExFilePicker activity will called.

* setNewFolderButtonDisabled() - if true, button "New folder" will not showing. You can also remove WRITE_EXTERNAL_STORAGE permission in this case.

* setSortButtonDisabled() - if true, button "Sort" will not showing.

* setQuitButtonEnabled() - if true, quit button will showing.

* setSortingType() - one of value from SortingType. Set default sorting. NAME_ASC by default.

* setUseFirstItemAsUpEnabled() - enable link to the parent directory as first item in list. 

Feel free to look sample.

## Customization

ExFilePicker library provide two themes for ExFilePicker's activity: dark and light (ExFilePickerThemeDark and ExFilePickerThemeLight respectively). If you need to customize ExFilePicker's activity view, you can extend your own theme from any ExFilePicker's theme and override needed options. To set theme you need specify activity in Manifest:

```xml
<activity
    android:name="com.rilixtech.exfilepicker.ui.activity.ExFilePickerActivity"
    android:theme="@style/ExFilePickerThemeDark"
    tools:replace="android:theme"/>
```
