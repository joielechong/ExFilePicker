package com.rilixtech.exfilepicker.adapter.holder;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.view.View;
import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.rilixtech.exfilepicker.OnListItemClickListener;
import com.rilixtech.exfilepicker.R;
import com.rilixtech.exfilepicker.utils.Utils;
import java.io.File;

import static com.bumptech.glide.request.RequestOptions.centerCropTransform;

/**
 * Created by BArtWell on 26.02.2017.
 */
public class FileFilesListHolder extends BaseFilesListHolder {
  @NonNull private final Context mContext;
  @Nullable private final AppCompatTextView mFileSize;
  @NonNull private final AppCompatImageView mThumbnail;

  public FileFilesListHolder(@NonNull View itemView) {
    super(itemView);
    mContext = itemView.getContext();
    mFileSize = itemView.findViewById(R.id.filesize);
    mThumbnail = itemView.findViewById(R.id.thumbnail);
  }

  @Override
  public void bind(@NonNull File file, boolean isMultiChoiceModeEnabled, boolean isSelected,
      @Nullable OnListItemClickListener listener) {
    super.bind(file, isMultiChoiceModeEnabled, isSelected, listener);
    if (mFileSize != null) {
      mFileSize.setVisibility(View.VISIBLE);
      mFileSize.setText(Utils.getHumanReadableFileSize(mContext, file.length()));
    }

    Glide.with(mContext)
        .load(file)
        .apply(centerCropTransform()
            .error(R.drawable.efp__ic_file)
            .priority(Priority.HIGH))
        .into(mThumbnail);
  }
}
