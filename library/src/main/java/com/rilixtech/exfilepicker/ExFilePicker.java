package com.rilixtech.exfilepicker;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.util.Observable;
import java.util.Observer;

/**
 * Created by BArtWell on 26.02.2017.
 */

public class ExFilePicker implements Observer {
  private Context mContext;
  private boolean mCanChooseOnlyOneItem;
  private String[] mShowOnlyExtensions;
  private String[] mExceptExtensions;
  private boolean mIsNewFolderButtonDisabled;
  private boolean mIsSortButtonDisabled;
  private boolean mIsQuitButtonEnabled;
  private ChoiceType mChoiceType;
  private SortingType mSortingType;
  private String mStartDirectory;
  private boolean mUseFirstItemAsUpEnabled;
  private boolean mHideHiddenFilesEnabled;

  private ExFilePickerListener mListener;

  public enum ChoiceType {
    ALL, FILES, DIRECTORIES
  }

  public enum SortingType {
    NAME_ASC, NAME_DESC, SIZE_ASC, SIZE_DESC, DATE_ASC, DATE_DESC
  }

  public ExFilePicker resultListener(ExFilePickerListener listener) {
    this.mListener = listener;
    return this;
  }

  public interface ExFilePickerListener {
    void onResult(ExFilePickerResult result);
  }

  public static ExFilePicker with(Context context) {
    return new ExFilePicker(context);
  }

  public static ExFilePicker with(@NonNull android.support.v4.app.Fragment fragment) {
    return new ExFilePicker(fragment.getContext());
  }

  public static ExFilePicker with(@NonNull android.app.Fragment fragment) {
    Context context;
    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
      context = fragment.getContext();
    } else {
      context = fragment.getActivity();
    }
    return new ExFilePicker(context);
  }

  private ExFilePicker(Context context) {
    mContext = context;
    mCanChooseOnlyOneItem = false;
    mShowOnlyExtensions = null;
    mExceptExtensions = null;
    mIsNewFolderButtonDisabled = false;
    mIsSortButtonDisabled = false;
    mIsQuitButtonEnabled = false;
    mChoiceType = ChoiceType.ALL;
    mSortingType = SortingType.NAME_ASC;
    mStartDirectory = null;
    mUseFirstItemAsUpEnabled = false;
    mHideHiddenFilesEnabled = false;
  }
  public ExFilePicker setCanChooseOnlyOneItem(boolean canChooseOnlyOneItem) {
    mCanChooseOnlyOneItem = canChooseOnlyOneItem;
    return this;
  }

  public ExFilePicker setShowOnlyExtensions(@Nullable String... extension) {
    mShowOnlyExtensions = extension;
    return this;
  }

  public ExFilePicker setExceptExtensions(@Nullable String... extension) {
    mExceptExtensions = extension;
    return this;
  }

  public ExFilePicker setNewFolderButtonDisabled(boolean disabled) {
    mIsNewFolderButtonDisabled = disabled;
    return this;
  }

  public ExFilePicker setSortButtonDisabled(boolean disabled) {
    mIsSortButtonDisabled = disabled;
    return this;
  }

  public ExFilePicker setQuitButtonEnabled(boolean enabled) {
    mIsQuitButtonEnabled = enabled;
    return this;
  }

  public ExFilePicker setChoiceType(@NonNull ChoiceType choiceType) {
    mChoiceType = choiceType;
    return this;
  }

  public ExFilePicker setSortingType(@NonNull SortingType sortingType) {
    mSortingType = sortingType;
    return this;
  }

  public ExFilePicker setStartDirectory(@Nullable String startDirectory) {
    mStartDirectory = startDirectory;
    return this;
  }

  public ExFilePicker setUseFirstItemAsUpEnabled(boolean enabled) {
    mUseFirstItemAsUpEnabled = enabled;
    return this;
  }

  public ExFilePicker setHideHiddenFilesEnabled(boolean enabled) {
    mHideHiddenFilesEnabled = enabled;
    return this;
  }

  public void show() {
    mContext.startActivity(createIntent(mContext));
    ExFilePickerObservable.getInstance().addObserver(this);
  }

  @NonNull
  private Intent createIntent(@NonNull Context context) {
    Intent intent = new Intent(context, ExFilePickerActivity.class);
    intent.putExtra(ExFilePickerActivity.EXTRA_CAN_CHOOSE_ONLY_ONE_ITEM, mCanChooseOnlyOneItem);
    intent.putExtra(ExFilePickerActivity.EXTRA_SHOW_ONLY_EXTENSIONS, mShowOnlyExtensions);
    intent.putExtra(ExFilePickerActivity.EXTRA_EXCEPT_EXTENSIONS, mExceptExtensions);
    intent.putExtra(ExFilePickerActivity.EXTRA_IS_NEW_FOLDER_BUTTON_DISABLED, mIsNewFolderButtonDisabled);
    intent.putExtra(ExFilePickerActivity.EXTRA_IS_SORT_BUTTON_DISABLED, mIsSortButtonDisabled);
    intent.putExtra(ExFilePickerActivity.EXTRA_IS_QUIT_BUTTON_ENABLED, mIsQuitButtonEnabled);
    intent.putExtra(ExFilePickerActivity.EXTRA_CHOICE_TYPE, mChoiceType);
    intent.putExtra(ExFilePickerActivity.EXTRA_SORTING_TYPE, mSortingType);
    intent.putExtra(ExFilePickerActivity.EXTRA_START_DIRECTORY, mStartDirectory);
    intent.putExtra(ExFilePickerActivity.EXTRA_USE_FIRST_ITEM_AS_UP_ENABLED, mUseFirstItemAsUpEnabled);
    intent.putExtra(ExFilePickerActivity.EXTRA_HIDE_HIDDEN_FILES, mHideHiddenFilesEnabled);
    return intent;
  }

  @Override public void update(Observable o, Object arg) {
    ExFilePickerResult result = (ExFilePickerResult) arg;
    if(mListener != null) mListener.onResult(result);
  }
}
