package com.rilixtech.exfilepicker;

import java.util.Observable;

class ExFilePickerObservable extends Observable {
  private static ExFilePickerObservable instance;

  private ExFilePickerObservable(){}

  static ExFilePickerObservable getInstance() {
    if(instance == null) {
      synchronized (ExFilePickerObservable.class) {
        if(instance == null) {
          instance = new ExFilePickerObservable();
        }
      }
    }

    return instance;
  }

  void updateResult(ExFilePickerResult result) {
    synchronized (this) {
      setChanged();
      notifyObservers(result);
    }
  }
}
