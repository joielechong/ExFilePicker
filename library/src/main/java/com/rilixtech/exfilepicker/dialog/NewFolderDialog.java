package com.rilixtech.exfilepicker.dialog;

import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.widget.TextView;
import com.rilixtech.exfilepicker.R;

/**
 * Created by BArtWell on 01.03.2017.
 */
public class NewFolderDialog implements DialogInterface.OnClickListener {
  @Nullable private OnNewFolderNameEnteredListener mListener;
  private AlertDialog.Builder mAlert;

  public NewFolderDialog(@NonNull Context context) {
    mAlert = new AlertDialog.Builder(context)
        .setTitle(R.string.efp__new_folder)
        .setView(LayoutInflater.from(context).inflate(R.layout.efp__new_folder, null))
        .setPositiveButton(android.R.string.ok, this)
        .setNegativeButton(android.R.string.cancel, null);
  }

  public void setOnNewFolderNameEnteredListener(OnNewFolderNameEnteredListener listener) {
    mListener = listener;
  }

  @Override
  public void onClick(DialogInterface dialogInterface, int i) {
    TextView name = ((AlertDialog) dialogInterface).findViewById(R.id.name);
    if (mListener != null && name != null) {
      mListener.onNewFolderNameEntered(name.getText().toString());
    }
  }

  public void show() {
    mAlert.show();
  }

  public interface OnNewFolderNameEnteredListener {
    void onNewFolderNameEntered(@NonNull String name);
  }
}
